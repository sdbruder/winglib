<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

define('URL_SEPARATOR', '/');

class Wing extends Model
{

    protected $fillable = [
        'slug',
        'user_id',
        'name',
        'description',
        'image_file',
        'unitsystem',
        'cgpos',
        'weight',
        'drawvalues',
        'rootchord',
        'totalspan',
        'panels',
        'wingarea',
        'macdist',
        'maclength',
        'cg',
        'wingload',

        'panel1span',
        'panel1chord',
        'panel1sweep',
        'panel1angle',

        'panel2span',
        'panel2chord',
        'panel2sweep',
        'panel2angle',

        'panel3span',
        'panel3chord',
        'panel3sweep',
        'panel3angle',

        'panel4span',
        'panel4chord',
        'panel4sweep',
        'panel4angle',

        'panel5span',
        'panel5chord',
        'panel5sweep',
        'panel5angle',

        'panel6span',
        'panel6chord',
        'panel6sweep',
        'panel6angle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'panels' => 'array'
    ];

    protected $appends = [
        'panel1span',
        'panel1chord',
        'panel1sweep',
        'panel1angle',

        'panel2span',
        'panel2chord',
        'panel2sweep',
        'panel2angle',

        'panel3span',
        'panel3chord',
        'panel3sweep',
        'panel3angle',

        'panel4span',
        'panel4chord',
        'panel4sweep',
        'panel4angle',

        'panel5span',
        'panel5chord',
        'panel5sweep',
        'panel5angle',

        'panel6span',
        'panel6chord',
        'panel6sweep',
        'panel6angle'
    ];

    /**
     * boot method
     */
    protected static function boot()
    {
        parent::boot();

        // construct the panels json
        static::saving(function (Wing $wing) {
            if (array_key_exists('panel1span', $wing->attributes)) {
                $wing->panels = [
                    1 =>   ['span'  => (float) $wing->attributes['panel1span'],
                            'chord' => (float) $wing->attributes['panel1chord'],
                            'sweep' => (float) $wing->attributes['panel1sweep'],
                            'angle' => (float) $wing->attributes['panel1angle']],
                    2 =>   ['span'  => (float) $wing->attributes['panel2span'],
                            'chord' => (float) $wing->attributes['panel2chord'],
                            'sweep' => (float) $wing->attributes['panel2sweep'],
                            'angle' => (float) $wing->attributes['panel2angle']],
                    3 =>   ['span'  => (float) $wing->attributes['panel3span'],
                            'chord' => (float) $wing->attributes['panel3chord'],
                            'sweep' => (float) $wing->attributes['panel3sweep'],
                            'angle' => (float) $wing->attributes['panel3angle']],
                    4 =>   ['span'  => (float) $wing->attributes['panel4span'],
                            'chord' => (float) $wing->attributes['panel4chord'],
                            'sweep' => (float) $wing->attributes['panel4sweep'],
                            'angle' => (float) $wing->attributes['panel4angle']],
                    5 =>   ['span'  => (float) $wing->attributes['panel5span'],
                            'chord' => (float) $wing->attributes['panel5chord'],
                            'sweep' => (float) $wing->attributes['panel5sweep'],
                            'angle' => (float) $wing->attributes['panel5angle']],
                    6 =>   ['span'  => (float) $wing->attributes['panel6span'],
                            'chord' => (float) $wing->attributes['panel6chord'],
                            'sweep' => (float) $wing->attributes['panel6sweep'],
                            'angle' => (float) $wing->attributes['panel6angle']]
                ];
            }
            if ( ! $wing->slug ) {
                $slug_str = str_slug($wing->name);
                $latest = Wing::whereRaw("slug_str = '{$slug_str}'")->latest("slug_id")->get()->first();
                if ($latest) {
                    $slug_id = $latest['slug_id'] + 1;
                    $wing->attributes['slug_str'] = $slug_str;
                    $wing->attributes['slug_id'] = $slug_id;
                    $wing->attributes['slug'] = $slug_str . '-' . $slug_id;
                } else {
                    $wing->attributes['slug_str'] = $slug_str;
                    $wing->attributes['slug'] = $slug_str;
                }
            }
            $wing->attributes['totalspan'] =
                (
                floatval($wing->panels[1]['span']) +
                floatval($wing->panels[2]['span']) +
                floatval($wing->panels[3]['span']) +
                floatval($wing->panels[4]['span']) +
                floatval($wing->panels[5]['span']) +
                floatval($wing->panels[6]['span'])
                ) * 2;

            foreach($wing->attributes as $key => $value) {
                if (in_array($key,$wing->appends)) {
                    unset($wing->attributes[$key]);
                }
            }

            return true;
        }); // static::saving
    }

    public static function duplicateImage($orig_image) {
        $subdir = substr(microtime(),4,2);
        $storage_path = join(DIRECTORY_SEPARATOR, [storage_path(), 'app', 'wingimg', $subdir]);
        $url_path = join(URL_SEPARATOR, ['wingimg', $subdir]);
        $filename = uniqid('') . '.png';

        $image_filename = join(DIRECTORY_SEPARATOR, [ $storage_path, $filename]);
        $image_url      = URL_SEPARATOR . join(URL_SEPARATOR, [ $url_path, $filename]);

        $origin = join(DIRECTORY_SEPARATOR, [storage_path(), 'app', $orig_image]);
        @copy($origin, $image_filename);
        // return the URL made above.
        return $image_url;
    }

    public static function createImage($base64_image) {

        $subdir = substr(microtime(),4,2);
        $storage_path = join(DIRECTORY_SEPARATOR, [storage_path(), 'app', 'wingimg', $subdir]);
        $url_path = join(URL_SEPARATOR, ['wingimg', $subdir]);
        $filename = uniqid('') . '.png';

        $image_filename = join(DIRECTORY_SEPARATOR, [ $storage_path, $filename]);
        $image_url      = URL_SEPARATOR . join(URL_SEPARATOR, [ $url_path, $filename]);

        //get the base-64 from data
        $base64_str = substr($base64_image, strpos($base64_image, ",")+1);
        //decode base64 string and write it down.
        $image = base64_decode($base64_str);
        file_put_contents($image_filename, $image);

        // return the URL made above.
        return $image_url;
    }

    public function saveImage($base64_image) {
        $image_filename = join(DIRECTORY_SEPARATOR, [ storage_path(), 'app']) . $this->image_file;
        //get the base-64 from data
        $base64_str = substr($base64_image, strpos($base64_image, ",")+1);
        //decode base64 string and write it down.
        file_put_contents($image_filename, base64_decode($base64_str));
    }

    public function unit($what) {
        if ($this->unitsystem == 'metric') {
            $units = [  'distance'   => 'mm',
                        'weight'     => 'g',
                        'area'       => 'dm²',
                        'load'       => 'g/dm²',
                        'percentual' => '%',
                        'angle'      => '°'];
        } else {
            $units = [  'distance'   => 'in',
                        'weight'     => 'oz',
                        'area'       => 'in²',
                        'load'       => 'oz/ft²',
                        'percentual' => '%',
                        'angle'      => '°'];
        }
        return $units[$what];
    }

    public function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    public function clearDotZeroes($value) {
        if ($this->endsWith($value,'.00')) {
            $value = str_replace('.00', '', $value);
        }
        return $value;
    }

    public function getCGPosAttribute()
    {
        return $this->clearDotZeroes($this->attributes['cgpos']);
    }

    public function getPanel1SpanAttribute()
    {
        return $this->panels[1]['span'];
    }

    public function getPanel1ChordAttribute()
    {
        return $this->panels[1]['chord'];
    }

    public function getPanel1SweepAttribute()
    {
        return $this->panels[1]['sweep'];
    }

    public function getPanel1AngleAttribute()
    {
        return $this->panels[1]['angle'];
    }

    public function getPanel2SpanAttribute()
    {
        return $this->panels[2]['span'];
    }

    public function getPanel2ChordAttribute()
    {
        return $this->panels[2]['chord'];
    }

    public function getPanel2SweepAttribute()
    {
        return $this->panels[2]['sweep'];
    }

    public function getPanel2AngleAttribute()
    {
        return $this->panels[2]['angle'];
    }

    public function getPanel3SpanAttribute()
    {
        return $this->panels[3]['span'];
    }

    public function getPanel3ChordAttribute()
    {
        return $this->panels[3]['chord'];
    }

    public function getPanel3SweepAttribute()
    {
        return $this->panels[3]['sweep'];
    }

    public function getPanel3AngleAttribute()
    {
        return $this->panels[3]['angle'];
    }

    public function getPanel4SpanAttribute()
    {
        return $this->panels[4]['span'];
    }

    public function getPanel4ChordAttribute()
    {
        return $this->panels[4]['chord'];
    }

    public function getPanel4SweepAttribute()
    {
        return $this->panels[4]['sweep'];
    }

    public function getPanel4AngleAttribute()
    {
        return $this->panels[4]['angle'];
    }

    public function getPanel5SpanAttribute()
    {
        return $this->panels[5]['span'];
    }

    public function getPanel5ChordAttribute()
    {
        return $this->panels[5]['chord'];
    }

    public function getPanel5SweepAttribute()
    {
        return $this->panels[5]['sweep'];
    }

    public function getPanel5AngleAttribute()
    {
        return $this->panels[5]['angle'];
    }

    public function getPanel6SpanAttribute()
    {
        return $this->panels[6]['span'];
    }

    public function getPanel6ChordAttribute()
    {
        return $this->panels[6]['chord'];
    }

    public function getPanel6SweepAttribute()
    {
        return $this->panels[6]['sweep'];
    }

    public function getPanel6AngleAttribute()
    {
        return $this->panels[6]['angle'];
    }

    /**
     * prepare the Panels array
     * @return array return the Panels Array
     */
    private function prepPanelsArray()
    {
        $panels = $this->panels;
        if (gettype($panels) != 'array') {
            $panels = [];
        }
        for($p = 1; $p <= 6; $p++) {
            if (!array_key_exists($p, $panels)) {
                $panels[$p] = [
                    'span'  => 0.00,
                    'chord' => 0.00,
                    'sweep' => 0.00,
                    'angle' => 0.00];
            }
        }
        $this->attributes['panels'] = json_encode($panels);
        return $panels;
    }

    /**
     * A wing is owned by an user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * A wing was liked by many users (many to many)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likedBy()
    {
        return $this->belongsToMany('App\User', 'users_likes_wings');
    }
}
